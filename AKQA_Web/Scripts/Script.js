﻿function ajaxMethod(param) {
    var baseUrl = "http://localhost:61780/api/service?";
    $.getJSON(baseUrl + param, function (data) {
        if (data == null) {
            alert("Cannot process at the moment");
            return;
        }
        $("#dvResult").show();
        $("#lblName").text(data.Name);
        $("#lblNumber").text(data.Number);
    })
        .error(function () { alert("Error occurred, service may not available"); });

}
function NumAndTwoDecimals(e, field) {
    var val = field.value;
    var re = /^([0-9]+[\.]?[0-9]?[0-9]?|[0-9]+)$/g;
    var re1 = /^([0-9]+[\.]?[0-9]?[0-9]?|[0-9]+)/g;
    if (!re.test(val)) {
        val = re1.exec(val);
        if (val) {
            field.value = val[0];
        } else {
            field.value = "";
        }
    }
}
