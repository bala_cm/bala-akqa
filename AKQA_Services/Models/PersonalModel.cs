﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AKQA_Services.Models
{
    public class PersonalModel
    {
        public string Name { get; set; }

        public string Number { get; set; }
    }
}