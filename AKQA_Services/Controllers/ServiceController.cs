﻿using AKQA_Services.Helper;
using AKQA_Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace AKQA_Services.Controllers
{
    public class ServiceController : ApiController
    {
        public PersonalModel GetPersonal(string name, string number)
        {

            try
            {
                if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(number))
                {
                    double decNum;
                    int num;
                    string word = "Invalid number";
                    if (int.TryParse(number, out num))
                        word = HelperMethods.NumberToWords(num);
                    else if (double.TryParse(number, out decNum))
                        word = HelperMethods.NumberToWords(decNum);

                    return new PersonalModel { Name = name, Number = word };

                }
                return null;
            }
            catch (Exception ex)
            {
                //write log
                return null;
            }
            
            
        }

        


    }
}
